import {Component, Input} from '@angular/core';
import {LoaderStatus} from './loader.status';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  public LoaderStatus = LoaderStatus;
  @Input() public status: LoaderStatus;
}
