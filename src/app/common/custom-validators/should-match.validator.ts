import {FormGroup} from '@angular/forms';

export function shouldMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }

    matchingControl.setErrors(control.value !== matchingControl.value ? {shouldMatch: true} : null);
  };
}
