import {BrowserModule} from '@angular/platform-browser';
import {ANALYZE_FOR_ENTRY_COMPONENTS, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, RouterStateSnapshot, ROUTES} from '@angular/router';
import {routes} from './routes';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from './reducer';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EffectsModule} from '@ngrx/effects';
import {HttpWrapper} from './wrapper/http.wrapper';
import {AuthGuard} from './guards/auth.guard';
import {RedirectFromLoginPageGuard} from './guards/redirect-from-login-page.guard';
import {AuthModule} from './auth/auth.module';
import {CheckoutDialogComponent} from './basket/dialogs/checkout-dialog/checkout-dialog.component';
import {DialogsService} from './dialogs/dialogs.service';
import {MatDialogModule, MatSelectModule} from '@angular/material';
import {BasketModule} from './basket/basket.module';
import {UiEffects} from './ui/actions/effects/ui.effects';
import {LoaderModule} from './common/loader/loader.module';

export interface RouterStateUrl {
  url: string;
}

export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const {url} = routerState;

    return {url};
  }
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot(reducers, {
      metaReducers
    }),
    EffectsModule.forRoot([UiEffects]),
    StoreDevtoolsModule,
    StoreRouterConnectingModule.forRoot(),
    HttpClientModule,
    FormsModule,
    AuthModule,
    BasketModule,
    LoaderModule,
    MatSelectModule,
    MatDialogModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('x-access-token');
        },
        whitelistedDomains: ['localhost:3000']
      }
    }),
  ],
  declarations: [
    AppComponent,
    CheckoutDialogComponent
  ],
  providers: [
    RedirectFromLoginPageGuard,
    AuthGuard,
    HttpWrapper,
    JwtHelperService,
    DialogsService,
    {
      provide: RouterStateSerializer,
      useClass: CustomRouterStateSerializer
    },
    {
      provide: ROUTES,
      useValue: routes,
      multi: true
    },
    {
      provide: ANALYZE_FOR_ENTRY_COMPONENTS,
      useValue: routes,
      multi: true
    }
  ],
  entryComponents: [
    CheckoutDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

