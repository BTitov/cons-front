import {CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtHelperService) {
  }

  canActivate(): boolean {
    const token: string = localStorage.getItem('x-access-token');
    if (token && !this.jwtService.isTokenExpired(token)) {
      return true;
    }
    localStorage.removeItem('x-access-token');
    this.router.navigate(['login']);
    return false;
  }
}
