import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class RedirectFromLoginPageGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtHelperService) {
  }

  canActivate(): boolean {
    const token: string = localStorage.getItem('x-access-token');
    if (token && !this.jwtService.isTokenExpired(token)) {
      this.router.navigate(['/basket']);
      return false;
    } else {
      localStorage.removeItem('x-access-token');
      return true;
    }
  }

}
