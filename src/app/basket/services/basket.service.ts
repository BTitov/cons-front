import {Injectable} from '@angular/core';
import {HttpWrapper} from '../../wrapper/http.wrapper';
import {Observable} from 'rxjs';
import {Item} from '../models/item';
import {map} from 'rxjs/operators';
import {Card} from '../models/card';

@Injectable()
export class BasketService {
  constructor(private httpWrapper: HttpWrapper) {
  }

  public loadItems(): Observable<Item[]> {
    return this.httpWrapper.get('/items', {headers: {'x-access-token': localStorage.getItem('x-access-token')}})
      .pipe(map(items => items.map(item => new Item(item))));
  }

  public loadCards(): Observable<Card[]> {
    return this.httpWrapper.get('/cards', {headers: {'x-access-token': localStorage.getItem('x-access-token')}})
      .pipe(map(cards => cards.map(card => new Card(card))));
  }

  public addCard(card: Card): Observable<Card> {
    return this.httpWrapper.post('/cards', card, {headers: {'x-access-token': localStorage.getItem('x-access-token')}})
      .pipe(map(result => new Card(result)));
  }

  public pay(cardId: number): Observable<void> {
    return this.httpWrapper.get(`/paying/${cardId}`, {headers: {'x-access-token': localStorage.getItem('x-access-token')}});
  }
}
