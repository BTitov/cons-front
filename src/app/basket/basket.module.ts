import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BasketComponent} from './components/basket/basket.component';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import * as fromBasket from './actions/reducer/basket.reducer';
import {EffectsModule} from '@ngrx/effects';
import {basketRoutes} from './routes';
import {BasketEffects} from './actions/effects/basket.effects';
import {BasketService} from './services/basket.service';
import {AddCardComponent} from './components/add-card/add-card.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CreditCardDirectivesModule} from 'angular-cc-library';
import {LoaderModule} from '../common/loader/loader.module';

@NgModule({
  declarations: [BasketComponent, AddCardComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CreditCardDirectivesModule,
    RouterModule.forChild(basketRoutes),
    StoreModule.forFeature('basket', fromBasket.basketReducer),
    EffectsModule.forFeature([BasketEffects]),
    LoaderModule
  ],
  providers: [
    BasketService
  ],
  exports: [AddCardComponent]
})
export class BasketModule {
}
