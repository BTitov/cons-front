import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CreditCardValidator} from 'angular-cc-library';
import {Card} from '../../models/card';
import {LoaderStatus} from '../../../common/loader/loader.status';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent {
  public cardForm: FormGroup;

  @Input() public error: string;
  @Input() public loaderStatus: LoaderStatus;

  @Output() public onSave: EventEmitter<Card> = new EventEmitter();
  @Output() public onCancel: EventEmitter<void> = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {
    this.cardForm = this.formBuilder.group({
      owner: new FormControl('', [
        Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern(/^[A-Za-z ]+$/)
      ]),
      number: new FormControl('', [CreditCardValidator.validateCCNumber]),
      cvv: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(4)]),
      date: new FormControl('', [CreditCardValidator.validateExpDate])
    });
  }

  public getControls(): {[key: string]: AbstractControl} {
    return this.cardForm.controls;
  }

  public save(): void {
    this.onSave.emit({
      owner: this.cardForm.value.owner,
      number: this.cardForm.value.number,
      cvv: this.cardForm.value.cvv,
      expirationDate: this.cardForm.value.date
    } as Card);
  }

  public cancel(): void {
    this.onCancel.emit();
  }
}
