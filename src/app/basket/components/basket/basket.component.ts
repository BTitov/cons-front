import {ChangeDetectorRef, Component} from '@angular/core';
import {Subscription} from 'rxjs';
import {Item} from '../../models/item';
import {Store} from '@ngrx/store';
import * as fromBasket from '../../actions/reducer/index';
import {GetItemsAction} from '../../actions/basket.actions';
import {ShowDialogAction} from '../../../ui/actions/ui.actions';
import {DialogType} from '../../../dialogs/dialog-type.enum';
import {LogoutAction} from '../../../auth/actions/auth.actions';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent {
  public items: Item[];
  private readonly itemsSubscription: Subscription;

  constructor(private store: Store<fromBasket.State>,
              private chRef: ChangeDetectorRef) {
    this.store.dispatch(new GetItemsAction());
    this.itemsSubscription = this.store.select(fromBasket.getItems)
      .subscribe(items => {
        this.items = items || [];
        this.chRef.markForCheck();
      });
  }

  public openCheckoutDialog(): void {
    let price = 0;
    this.items.forEach(item => price += item.price);
    this.store.dispatch(new ShowDialogAction({type: DialogType.CheckoutDialog, payload: price}));
  }

  public logout(): void {
    this.store.dispatch(new LogoutAction());
  }
}
