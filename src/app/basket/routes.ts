import {Routes} from '@angular/router';
import {BasketComponent} from './components/basket/basket.component';

export const basketRoutes: Routes = [
  {
    path: '',
    component: BasketComponent
  }
];
