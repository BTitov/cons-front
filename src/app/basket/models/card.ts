export class Card {
  public id: number;
  public owner: string;
  public number: string;
  public cvv: string;
  public expirationDate: Date;

  constructor(json: any) {
    this.parse(json);
  }

  private parse(json: any) {
    if (json.id) {
      this.id = json.id;
    }
    if (json.owner) {
      this.owner = json.owner;
    }
    if (json.number) {
      this.number = json.number;
    }
    if (json.cvv) {
      this.cvv = json.cvv;
    }
    if (json.expirationDate) {
      this.expirationDate = json.expirationDate;
    }
  }
}
