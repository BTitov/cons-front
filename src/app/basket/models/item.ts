export class Item {
  public id: number;
  public name: string;
  public price: number;

  constructor(json: any) {
    this.parse(json);
  }

  private parse(json: any) {
    if (json.id) {
      this.id = json.id;
    }
    if (json.name) {
      this.name = json.name;
    }
    if (json.price) {
      this.price = json.price;
    }
  }
}
