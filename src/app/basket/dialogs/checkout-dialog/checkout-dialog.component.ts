import {ChangeDetectorRef, Component, Inject, OnDestroy, Renderer2} from '@angular/core';
import {BaseDialog} from '../../../dialogs/base-dialog/base-dialog';
import {OverlayContainer} from '@angular/cdk/overlay';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from '@angular/material';
import {DialogResult} from '../../../dialogs/dialog-result';
import {Store} from '@ngrx/store';
import {
  AddCardAction,
  GetCardsAction,
  HideAddCardComponentAction,
  PayAction,
  ShowAddCardComponentAction
} from '../../actions/basket.actions';
import {Observable, Subscription} from 'rxjs';
import {Card} from '../../models/card';
import {getCards, getShowAddCardComponent} from '../../actions/reducer';
import {filter} from 'rxjs/operators';
import {LoaderStatus} from '../../../common/loader/loader.status';
import {getError, getLoaderStatus} from '../../../ui/actions/reducers';
import {Actions, ofType} from '@ngrx/effects';
import {ClearLoaderStatusAction, UiActionTypes} from '../../../ui/actions/ui.actions';

@Component({
  selector: 'app-checkout-dialog',
  templateUrl: './checkout-dialog.component.html',
  styleUrls: ['./checkout-dialog.component.scss']
})
export class CheckoutDialogComponent extends BaseDialog implements OnDestroy {

  public cards: Card[];
  public showAddCardComp$: Observable<boolean>;
  public selected: Card;
  public loaderStatus$: Observable<LoaderStatus>;
  public error$: Observable<string>;

  private readonly actionsSubscription: Subscription;
  private readonly cardsSubscription: Subscription;

  constructor(overlayContainer: OverlayContainer, renderer: Renderer2,
              public dialogRef: MatDialogRef<CheckoutDialogComponent, DialogResult>,
              private store: Store<any>, @Inject(MAT_DIALOG_DATA) public price: number,
              private cdRef: ChangeDetectorRef, private actions: Actions) {
    super(overlayContainer, renderer, dialogRef);
    this.store.dispatch(new GetCardsAction());
    this.cardsSubscription = this.store.select(getCards)
      .pipe(filter(cards => !!cards && !!cards.length))
      .subscribe((cards: Card[]) => {
        this.cards = cards;
        this.selected = cards[0];
        this.cdRef.markForCheck();
      });
    this.loaderStatus$ = this.store.select(getLoaderStatus);
    this.error$ = this.store.select(getError);
    this.showAddCardComp$ = this.store.select(getShowAddCardComponent);

    this.actionsSubscription = this.actions.pipe(
      ofType(UiActionTypes.ClearLoaderStatus),
      filter((action: ClearLoaderStatusAction) => action.closeDialog))
      .subscribe(() => this.onClose());
  }

  public ngOnDestroy(): void {
    if (this.cardsSubscription) {
      this.cardsSubscription.unsubscribe();
    }
    if (this.actionsSubscription) {
      this.actionsSubscription.unsubscribe();
    }
  }

  public showAddCardComponent() {
    this.store.dispatch(new ShowAddCardComponentAction());
  }

  public hideAddCardComponent() {
    this.store.dispatch(new HideAddCardComponentAction());
  }

  public onSelected($event: MatSelectChange): void {
    this.selected = $event.value;
  }

  public addCard(card: Card): void {
    this.store.dispatch(new AddCardAction(card));
  }

  public pay(): void {
    this.store.dispatch(new PayAction(this.selected.id));
  }

}
