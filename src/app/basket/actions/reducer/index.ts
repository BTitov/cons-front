import * as fromBasket from './basket.reducer';
import * as fromRoot from '../../../reducer';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State extends fromRoot.State {
  basket: fromBasket.State;
}

export const getBasketState = createFeatureSelector<fromBasket.State>('basket');

export const getItems = createSelector(
  getBasketState,
  (state: fromBasket.State) => state.items
);

export const getCards = createSelector(
  getBasketState,
  (state: fromBasket.State) => state.cards
);

export const getShowAddCardComponent = createSelector(
  getBasketState,
  (state: fromBasket.State) => state.showAddCardComponent
);
