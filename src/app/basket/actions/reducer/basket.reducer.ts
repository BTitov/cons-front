import {Item} from '../../models/item';
import {Card} from '../../models/card';
import {BasketActions, BasketActionTypes} from '../basket.actions';
import {UiActions, UiActionTypes} from '../../../ui/actions/ui.actions';
import {AuthActions, AuthActionTypes} from '../../../auth/actions/auth.actions';

export interface State {
  items: Item[];
  cards: Card[];
  showAddCardComponent: boolean;
}

export const initialState = {
  items: [],
  cards: [],
  showAddCardComponent: false
};

export function basketReducer(state: State = initialState, action: BasketActions | UiActions | AuthActions): State {
  switch (action.type) {
    case BasketActionTypes.GetItemsSuccess:
      return {
        ...state,
        items: action.items
      };
    case BasketActionTypes.GetCardsSuccess:
      return {
        ...state,
        cards: action.cards
      };
    case BasketActionTypes.AddCardSuccess:
      return {
        ...state,
        cards: [action.card, ...state.cards]
      };
    case UiActionTypes.ClearLoaderStatus:
      return {
        ...state,
        items: action.closeDialog ? [] : state.items,
        showAddCardComponent: false
      };
    case UiActionTypes.CloseDialog:
    case BasketActionTypes.HideAddCardComponent:
      return {
        ...state,
        showAddCardComponent: false
      };
    case BasketActionTypes.ShowAddCardComponent:
      return {
        ...state,
        showAddCardComponent: true
      };
    case AuthActionTypes.Logout:
      return initialState;
    default:
      return state;
  }
}
