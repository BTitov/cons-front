import {Action} from '@ngrx/store';
import {Item} from '../models/item';
import {Card} from '../models/card';

export enum BasketActionTypes {
  GetItems = '[Basket] get items action',
  GetItemsSuccess = '[Basket] get items success action',
  GetCards = '[Basket] get cards action',
  GetCardsSuccess = '[Basket] get cards success action',
  AddCard = '[Basket] add card action',
  AddCardSuccess = '[Basket] add card success action',
  AddCardFailure = '[Basket] add card failure action',
  Pay = '[Basket] pay action',
  PaySuccess = '[Basket] pay success action',
  PayFailure = '[Basket] pay failure action',
  ShowAddCardComponent = '[Basket] show add card component action',
  HideAddCardComponent = '[Basket] hide add card component action'
}

export class GetItemsAction implements Action {
  public readonly type = BasketActionTypes.GetItems;
}

export class GetItemsSuccessAction implements Action {
  public readonly type = BasketActionTypes.GetItemsSuccess;

  constructor(public items: Item[]) {
  }
}

export class GetCardsAction implements Action {
  public readonly type = BasketActionTypes.GetCards;
}

export class GetCardsSuccessAction implements Action {
  public readonly type = BasketActionTypes.GetCardsSuccess;

  constructor(public cards: Card[]) {
  }
}

export class AddCardAction implements Action {
  public readonly type = BasketActionTypes.AddCard;

  constructor(public card: Card) {
  }
}

export class AddCardSuccessAction implements Action {
  public readonly type = BasketActionTypes.AddCardSuccess;

  constructor(public card: Card) {
  }
}

export class AddCardFailureAction implements Action {
  public readonly type = BasketActionTypes.AddCardFailure;

  constructor(public error: string) {
  }
}

export class ShowAddCardComponentAction implements Action {
  public readonly type = BasketActionTypes.ShowAddCardComponent;
}

export class HideAddCardComponentAction implements Action {
  public readonly type = BasketActionTypes.HideAddCardComponent;
}

export class PayAction implements Action {
  public readonly type = BasketActionTypes.Pay;

  constructor(public cardId: number) {
  }
}

export class PaySuccessAction implements Action {
  public readonly type = BasketActionTypes.PaySuccess;
}

export class PayFailureAction implements Action {
  public readonly type = BasketActionTypes.PayFailure;

  constructor(public error: string) {
  }
}


export type BasketActions =
  GetItemsAction |
  GetItemsSuccessAction |
  GetCardsAction |
  GetCardsSuccessAction |
  AddCardAction |
  AddCardSuccessAction |
  AddCardFailureAction |
  ShowAddCardComponentAction |
  HideAddCardComponentAction |
  PayAction |
  PaySuccessAction |
  PayFailureAction;
