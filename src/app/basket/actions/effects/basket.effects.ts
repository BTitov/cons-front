import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {BasketService} from '../../services/basket.service';
import {Observable, of} from 'rxjs';
import {
  AddCardAction, AddCardFailureAction, AddCardSuccessAction,
  BasketActionTypes,
  GetCardsAction,
  GetCardsSuccessAction,
  GetItemsAction,
  GetItemsSuccessAction, PayAction, PayFailureAction, PaySuccessAction
} from '../basket.actions';
import {catchError, exhaustMap, map, switchMap} from 'rxjs/operators';
import {Action} from '@ngrx/store';

@Injectable()
export class BasketEffects {
  constructor(private actions$: Actions, private basketService: BasketService) {
  }

  @Effect()
  loadItems$: Observable<Action> = this.actions$.pipe(
    ofType(BasketActionTypes.GetItems),
    exhaustMap((action: GetItemsAction) =>
      this.basketService.loadItems().pipe(
        map(items => new GetItemsSuccessAction(items)),
        catchError(err => of(new GetItemsSuccessAction([]))))
    ));

  @Effect()
  loadCards$: Observable<Action> = this.actions$.pipe(
    ofType(BasketActionTypes.GetCards),
    exhaustMap((action: GetCardsAction) =>
      this.basketService.loadCards().pipe(
        map(cards => new GetCardsSuccessAction(cards)),
        catchError(err => of(new GetCardsSuccessAction([]))))
    ));

  @Effect()
  addCard$: Observable<Action> = this.actions$.pipe(
    ofType(BasketActionTypes.AddCard),
    switchMap((action: AddCardAction) =>
      this.basketService.addCard(action.card).pipe(
        map(card => new AddCardSuccessAction(card)),
        catchError(res => of(new AddCardFailureAction(res.error.message))))
    ));

  @Effect()
  pay$: Observable<Action> = this.actions$.pipe(
    ofType(BasketActionTypes.Pay),
    switchMap((action: PayAction) =>
      this.basketService.pay(action.cardId).pipe(
        map(() => new PaySuccessAction()),
        catchError(res => of(new PayFailureAction(res.error.message))))
    ));
}
