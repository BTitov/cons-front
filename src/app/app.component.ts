import {Component, OnDestroy} from '@angular/core';
import {LoggedInCheckerService} from './auth/services/loggedin-checker.service';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import * as fromUi from './ui/actions/reducers/index';
import {DialogData} from './dialogs/dialog-data';
import {filter} from 'rxjs/operators';
import {DialogResult} from './dialogs/dialog-result';
import {CloseDialogAction} from './ui/actions/ui.actions';
import {DialogsService} from './dialogs/dialogs.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  private readonly dialogsSubscription: Subscription;
  constructor(private loggedInChecker: LoggedInCheckerService,
              public store: Store<any>,
              private dialogsService: DialogsService) {
    this.loggedInChecker.checkIfLoggedIn();

    this.dialogsSubscription = this.store.select(fromUi.getDialogData)
      .pipe(filter((dialogData: DialogData) => !!(dialogData && dialogData.type)))
      .subscribe((dialogData: DialogData) => {
        this.handleDialogs(dialogData);
      });
  }

  public ngOnDestroy(): void {
    if (this.dialogsSubscription) {
      this.dialogsSubscription.unsubscribe();
    }
  }

  private handleDialogs(dialogData: DialogData): void {
    this.dialogsService.getDialog(dialogData.type, dialogData.payload)
      .subscribe((result: DialogResult) => {
        if (result && result.action) {
          this.store.dispatch(result.action);
        }
        this.store.dispatch(new CloseDialogAction());
      });
  }
}
