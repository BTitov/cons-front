import {Component} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {RegisterAction} from '../../actions/auth.actions';
import {User} from '../../models/User';
import {shouldMatch} from '../../../common/custom-validators/should-match.validator';
import * as fromAuth from '../../actions/reducer/index';
import {Observable} from 'rxjs';
import {getError, getLoaderStatus} from '../../../ui/actions/reducers';
import {LoaderStatus} from '../../../common/loader/loader.status';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  public registerForm: FormGroup;
  public error$: Observable<string>;
  public loaderStatus$: Observable<LoaderStatus>;
  public LoaderStatus = LoaderStatus;

  constructor(private formBuilder: FormBuilder, private store: Store<fromAuth.State>) {
    this.registerForm = this.formBuilder.group({
      name: new FormControl('', [
        Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern(/^[A-Za-z ]+$/)
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)])
    }, {
      validator: shouldMatch('password', 'confirmPassword')
    });
    this.error$ = this.store.select(getError);
    this.loaderStatus$ = this.store.select(getLoaderStatus);
  }

  public getControls(): {[key: string]: AbstractControl} {
    return this.registerForm.controls;
  }

  public register(): void {
    this.store.dispatch(new RegisterAction({
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      name: this.registerForm.value.name
    } as User));
  }
}
