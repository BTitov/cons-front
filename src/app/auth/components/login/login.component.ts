import {Component} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {LoginAction} from '../../actions/auth.actions';
import {User} from '../../models/User';
import * as fromAuth from '../../actions/reducer/index';
import {Observable} from 'rxjs';
import {getError, getLoaderStatus} from '../../../ui/actions/reducers';
import {LoaderStatus} from '../../../common/loader/loader.status';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public loginForm: FormGroup;
  public error$: Observable<string>;
  public loaderStatus$: Observable<LoaderStatus>;

  constructor(private formBuilder: FormBuilder, private store: Store<fromAuth.State>) {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
    this.error$ = this.store.select(getError);
    this.loaderStatus$ = this.store.select(getLoaderStatus);
  }

  public getControls(): {[key: string]: AbstractControl} {
    return this.loginForm.controls;
  }

  public login(): void {
    this.store.dispatch(new LoginAction({email: this.loginForm.value.email, password: this.loginForm.value.password} as User));
  }
}
