import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './components/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {authRoutes} from './routes';
import {RegisterComponent} from './components/register/register.component';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './actions/effects/auth.effects';
import {AuthService} from './services/auth.service';
import * as fromAuth from './actions/reducer/auth.reducer';
import {LoggedInCheckerService} from './services/loggedin-checker.service';
import {LoaderModule} from '../common/loader/loader.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(authRoutes),
    StoreModule.forFeature('auth', fromAuth.authReducer),
    EffectsModule.forFeature([AuthEffects]),
    LoaderModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
  providers: [
    AuthService,
    LoggedInCheckerService
  ]
})
export class AuthModule {
}
