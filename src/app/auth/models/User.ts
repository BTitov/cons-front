export class User {
  public id: number;
  public name: string;
  public email: string;
  public password: string;

  constructor(json: any) {
    this.parse(json);
  }

  private parse(json: any) {
    if (json.id) {
      this.id = json.id;
    }
    if (json.name) {
      this.name = json.name;
    }
    if (json.email) {
      this.email = json.email;
    }
    if (json.password) {
      this.password = json.password;
    }
  }
}
