import {Injectable} from '@angular/core';
import {HttpWrapper} from '../../wrapper/http.wrapper';
import {User} from '../models/User';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthService {

  constructor(private httpWrapper: HttpWrapper) {
  }

  public login(user: User): Observable<boolean> {
    return this.httpWrapper.post('/login', user)
      .pipe(map(result => {
        if (result.token) {
          localStorage.setItem('x-access-token', result.token);
        }
        return !!result.token;
      }));
  }

  public register(user: User): Observable<User> {
    return this.httpWrapper.post('/register', user)
      .pipe(map(result => new User(result)));
  }
}
