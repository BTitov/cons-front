import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {SetLoginAction} from '../actions/auth.actions';

@Injectable()
export class LoggedInCheckerService {
  constructor(public store: Store<any>) {
  }

  public checkIfLoggedIn() {
    const token = localStorage.getItem('x-access-token');
    if (!token) {
      return;
    }
    this.store.dispatch(new SetLoginAction(true));
  }
}
