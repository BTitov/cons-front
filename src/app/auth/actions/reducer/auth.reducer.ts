import {AuthActions, AuthActionTypes} from '../auth.actions';

export interface State {
  loggedIn: boolean;
}

export const initialState: State = {
  loggedIn: false
};

export function authReducer(state: State = initialState, action: AuthActions): State {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess:
    case AuthActionTypes.SetLogin:
      return {
        ...state,
        loggedIn: action.loggedIn
      };
    case AuthActionTypes.Logout:
      return initialState;
    default:
      return state;
  }
}
