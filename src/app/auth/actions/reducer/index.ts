import * as fromAuth from './auth.reducer';
import * as fromRoot from '../../../reducer';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State extends fromRoot.State {
  auth: fromAuth.State;
}

export const getAuthState = createFeatureSelector<State>('auth');
