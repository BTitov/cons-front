import {Action} from '@ngrx/store';
import {User} from '../models/User';

export enum AuthActionTypes {
  Login = '[Auth] login action',
  LoginSuccess = '[Auth] login success action',
  LoginFailure = '[Auth] login failure action',
  Register = '[Auth] register action',
  RegisterSuccess = '[Auth] register success action',
  RegisterFailure = '[Auth] register failure action',
  SetLogin = '[Auth] set login action',
  Logout = '[Auth] logout action'
}

export class LoginAction implements Action {
  public readonly type = AuthActionTypes.Login;

  constructor(public user: User) {
  }
}

export class LoginSuccessAction implements Action {
  public readonly type = AuthActionTypes.LoginSuccess;

  constructor(public loggedIn: boolean) {
  }
}

export class LoginFailureAction implements Action {
  public readonly type = AuthActionTypes.LoginFailure;

  constructor(public error: string) {
  }
}

export class RegisterAction implements Action {
  public readonly type = AuthActionTypes.Register;

  constructor(public user: User) {
  }
}

export class RegisterSuccessAction implements Action {
  public readonly type = AuthActionTypes.RegisterSuccess;

  constructor(public user: User) {
  }
}

export class RegisterFailureAction implements Action {
  public readonly type = AuthActionTypes.RegisterFailure;

  constructor(public error: string) {
  }
}

export class SetLoginAction implements Action {
  public readonly type = AuthActionTypes.SetLogin;

  constructor(public loggedIn: boolean) {
  }
}

export class LogoutAction implements Action {
  public readonly type = AuthActionTypes.Logout;
}

export type AuthActions =
  LoginAction |
  LoginSuccessAction |
  LoginFailureAction |
  RegisterAction |
  RegisterSuccessAction |
  RegisterFailureAction |
  SetLoginAction |
  LogoutAction;
