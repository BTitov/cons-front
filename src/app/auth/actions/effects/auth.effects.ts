import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {
  AuthActionTypes,
  LoginAction,
  LoginFailureAction,
  LoginSuccessAction,
  LogoutAction,
  RegisterAction,
  RegisterFailureAction,
  RegisterSuccessAction
} from '../auth.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions,
              private store: Store<any>,
              private authService: AuthService,
              private router: Router) {
  }

  @Effect()
  login$: Observable<Action> = this.actions$.pipe(
    ofType(AuthActionTypes.Login),
    switchMap((action: LoginAction) =>
      this.authService.login(action.user).pipe(
        map(result => new LoginSuccessAction(result)),
        catchError(res => of(new LoginFailureAction(res.error.message))))
    ));

  @Effect()
  register$: Observable<Action> = this.actions$.pipe(
    ofType(AuthActionTypes.Register),
    switchMap((action: RegisterAction) =>
      this.authService.register(action.user).pipe(
        map(user => new RegisterSuccessAction(user)),
        catchError(res => of(new RegisterFailureAction(res.error.message))))
    ));

  @Effect({dispatch: false})
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    map((action: LogoutAction) => {
      localStorage.removeItem('x-access-token');
      this.router.navigate(['/login']);
    }));
}
