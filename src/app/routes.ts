import {Routes} from '@angular/router';
import {RedirectFromLoginPageGuard} from './guards/redirect-from-login-page.guard';
import {AuthGuard} from './guards/auth.guard';

export const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    canActivate: [RedirectFromLoginPageGuard]
  },
  /*{
    path: '**',
    redirectTo: 'login'
  },
  {
    path: '',
    redirectTo: './login',
    pathMatch: 'full'
  },*/
  {
    path: 'basket',
    loadChildren: () => import('./basket/basket.module').then(m => m.BasketModule),
    canActivate: [AuthGuard]
  }
];
