import {Action} from '@ngrx/store';
import {DialogData} from '../../dialogs/dialog-data';

export enum UiActionTypes {
  ShowDialog = '[Ui] Show dialog',
  CloseDialog = '[Ui] Close dialog',
  ClearError = '[Ui] Clear error',
  ClearLoaderStatus = '[Ui] Clear loader status'
}

export class ShowDialogAction implements Action {
  public readonly type = UiActionTypes.ShowDialog;

  constructor(public data: DialogData) {
  }
}

export class CloseDialogAction implements Action {
  public readonly type = UiActionTypes.CloseDialog;
}

export class ClearErrorAction implements Action {
  public readonly type = UiActionTypes.ClearError;
}

export class ClearLoaderStatusAction implements Action {
  public readonly type = UiActionTypes.ClearLoaderStatus;

  constructor(public closeDialog = false) {
  }
}

export type UiActions =
  ShowDialogAction |
  CloseDialogAction |
  ClearErrorAction |
  ClearLoaderStatusAction;
