import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {Action, Store} from '@ngrx/store';
import {AuthActionTypes} from '../../../auth/actions/auth.actions';
import {delay, filter, map, switchMap, tap, withLatestFrom} from 'rxjs/operators';
import {ClearErrorAction, ClearLoaderStatusAction, UiActionTypes} from '../ui.actions';
import {Router} from '@angular/router';
import {BasketActionTypes} from '../../../basket/actions/basket.actions';

@Injectable()
export class UiEffects {
  constructor(private actions$: Actions, private router: Router, private store: Store<any>) {
  }

  @Effect()
  clearError$: Observable<Action> = this.actions$.pipe(
    ofType(AuthActionTypes.LoginFailure, AuthActionTypes.RegisterFailure, BasketActionTypes.AddCardFailure, BasketActionTypes.PayFailure),
    switchMap(action => of(action).pipe(delay(3000))),
    map(() => new ClearErrorAction()));

  @Effect()
  clearLoader$: Observable<Action> = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess, AuthActionTypes.RegisterSuccess, BasketActionTypes.AddCardSuccess, BasketActionTypes.PaySuccess),
    switchMap(action => of(action).pipe(delay(1000))),
    map(() => new ClearLoaderStatusAction()));

  @Effect()
  clearLoaderAfterPay$: Observable<Action> = this.actions$.pipe(
    ofType(BasketActionTypes.PaySuccess),
    switchMap(action => of(action).pipe(delay(1000))),
    map(() => new ClearLoaderStatusAction(true)));

  @Effect({dispatch: false})
  redirectAfterSuccess$ = this.actions$.pipe(
    ofType(UiActionTypes.ClearLoaderStatus),
    withLatestFrom(this.store),
    filter(([action, store]) => store.uiReducer.redirectUrl),
    tap(([action, store]) => this.router.navigate([store.uiReducer.redirectUrl]))
  );

}
