import {DialogData} from '../../../dialogs/dialog-data';
import {UiActions, UiActionTypes} from '../ui.actions';
import {AuthActions, AuthActionTypes} from '../../../auth/actions/auth.actions';
import {ROUTER_NAVIGATED, RouterNavigatedAction} from '@ngrx/router-store';
import {LoaderStatus} from '../../../common/loader/loader.status';
import {BasketActions, BasketActionTypes} from '../../../basket/actions/basket.actions';

export interface State {
  dialogData: DialogData;
  loaderStatus: LoaderStatus;
  error: string;
  redirectUrl: string;
}

export const initialState: State = {
  dialogData: null,
  loaderStatus: null,
  error: null,
  redirectUrl: null
};

export function uiReducer(state: State = initialState,
                          action: UiActions | AuthActions | BasketActions | RouterNavigatedAction): State {
  switch (action.type) {
    case UiActionTypes.ShowDialog:
      return {
        ...state,
        dialogData: action.data
      };
    case UiActionTypes.CloseDialog:
      return {
        ...state,
        dialogData: null
      };
    case AuthActionTypes.RegisterFailure:
    case AuthActionTypes.LoginFailure:
    case BasketActionTypes.AddCardFailure:
    case BasketActionTypes.PayFailure:
      const splitted = action.error.split(': ');
      const lastMessage = splitted[splitted.length - 1];
      return {
        ...state,
        error: lastMessage,
        loaderStatus: null
      };
    case AuthActionTypes.Register:
    case AuthActionTypes.Login:
    case BasketActionTypes.AddCard:
    case BasketActionTypes.Pay:
      return {
        ...state,
        loaderStatus: LoaderStatus.Loading
      };
    case AuthActionTypes.RegisterSuccess:
      return {
        ...state,
        loaderStatus: LoaderStatus.Success,
        redirectUrl: '/login'
      };
    case AuthActionTypes.LoginSuccess:
      return {
        ...state,
        loaderStatus: LoaderStatus.Success,
        redirectUrl: '/basket'
      };
    case BasketActionTypes.AddCardSuccess:
      return {
        ...state,
        loaderStatus: LoaderStatus.Success
      };
    case ROUTER_NAVIGATED:
      return {
        ...state,
        error: null,
        redirectUrl: null
      };
    case UiActionTypes.ClearError:
      return {
        ...state,
        error: null
      };
    case UiActionTypes.ClearLoaderStatus:
      return {
        ...state,
        loaderStatus: null
      };
    case BasketActionTypes.PaySuccess:
      return {
        ...state,
        loaderStatus: LoaderStatus.Success
      };
    case AuthActionTypes.Logout:
      return initialState;
    default:
      return state;
  }
}
