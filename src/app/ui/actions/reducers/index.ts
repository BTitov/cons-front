import {State} from '../../../reducer';
import {createSelector} from '@ngrx/store';
import * as fromUi from './ui';

export const getUiState = (state: State) => state.uiReducer;

export const getDialogData = createSelector(
  getUiState,
  (uiState: fromUi.State) => uiState.dialogData
);

export const getError = createSelector(
  getUiState,
  (uiState: fromUi.State) => uiState.error
);

export const getLoaderStatus = createSelector(
  getUiState,
  (uiState: fromUi.State) => uiState.loaderStatus
);
