import {ActionReducer, ActionReducerMap, MetaReducer} from '@ngrx/store';
import {routerReducer, RouterReducerState} from '@ngrx/router-store';
import * as fromUi from '../ui/actions/reducers/ui';

export interface State {
  routerReducer: RouterReducerState;
  uiReducer: fromUi.State;
}

export const reducers: ActionReducerMap<State> = {
  routerReducer,
  uiReducer: fromUi.uiReducer
};

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state: State, action: any): State => {
    console.log('state', state);
    console.log('action', action);
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<State>[] = [logger];
