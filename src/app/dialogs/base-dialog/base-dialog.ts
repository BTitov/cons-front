import {OverlayContainer} from '@angular/cdk/overlay';
import {Renderer2} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {DialogResult} from '../dialog-result';

export class BaseDialog {

  protected dialogResult: DialogResult;

  constructor(overlayContainer: OverlayContainer, renderer: Renderer2,
              public dialogRef: MatDialogRef<BaseDialog, DialogResult>) {
    this.initResult();
    const overlayContainerElement: HTMLElement = overlayContainer.getContainerElement();
    renderer.setProperty(overlayContainerElement, '@.disabled', true);
  }

  public onCancel(): void {
    this.dialogRef.close(this.dialogResult);
  }

  public onConfirm(): void {
    this.dialogRef.close(this.dialogResult);
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  protected initResult(): void {
    this.dialogResult = {action: null} as DialogResult;
  }
}
