import {Action} from '@ngrx/store';

export interface DialogResult {
  action: Action;
}
