import {DialogType} from './dialog-type.enum';

export interface DialogData {
  type: DialogType;
  payload?: any;
}
