import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {DialogResult} from './dialog-result';
import {DialogType} from './dialog-type.enum';
import {BaseDialog} from './base-dialog/base-dialog';
import {Observable} from 'rxjs';
import {CheckoutDialogComponent} from '../basket/dialogs/checkout-dialog/checkout-dialog.component';

@Injectable()
export class DialogsService {
  constructor(private dialog: MatDialog) {
  }

  public getDialog(type: DialogType, data: any): Observable<DialogResult> {
    let dialogRef: MatDialogRef<BaseDialog, DialogResult>;
    switch (type) {
      case DialogType.CheckoutDialog:
        dialogRef = this.dialog.open(CheckoutDialogComponent, {
          width: '500px',
          position: {top: '200px'},
          data
        });
        break;
      default:
        break;
    }
    if (!dialogRef) {
      return;
    }
    return dialogRef.afterClosed();
  }
}
