import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class HttpWrapper {
  private baseApiUrl = 'http://localhost:3000';

  constructor(protected http: HttpClient) {
  }

  public post(url: string, body: any, options?: any): Observable<any> {
    return this.http.post<any>(this.baseApiUrl + url, body, options);
  }

  public put(url: string, body: any, options?: any): Observable<any> {
    return this.http.put<any>(this.baseApiUrl + url, body, options);
  }

  public get(url: string, options?: any): Observable<any> {
    return this.http.get<any>(this.baseApiUrl + url, options);
  }

  public delete(url: string, options?: any): Observable<any> {
    return this.http.delete<any>(this.baseApiUrl + url, options);
  }
}
